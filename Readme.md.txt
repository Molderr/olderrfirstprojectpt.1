Michael Olderr's project readme!

Projects objective: Scan through a txt file (might be bee movie script) and count the number of words as well as its five least and most used words.

It will run on java, with primary help from a scanner and maybe a hashmap

The program will contain a counter function with its scanner as well as function that will count what word is used 20+ time and what word is used at least 2 twice to determine usage. Will flex with count to get it close to 5